/* Copyright (c) 2010-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

/* This example shows how to simulate a non-linear resource sharing for
 * network links.
 */

#include <simgrid/s4u.hpp>
#include <simgrid/kernel/ProfileBuilder.hpp>
#include <time.h>
#include <random>
#include <vector>
#include <sstream>


namespace sg4 = simgrid::s4u;
namespace pr = simgrid::kernel::profile;

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_comm_failure2, "Messages specific for this s4u example");

/*************************************************************************************************/

//Constants for platform configuration

constexpr double HostComputePower=1e9; //FLOPs
constexpr double LinkBandwidth=1e9; //Bytes/second
constexpr double LinkLatency=1e-6;  //Seconds

//Constants for application behaviour

constexpr uint64_t  MsgSize=LinkBandwidth/2; 

/*************************************************************************************************/

enum class CommType {
	SYNC,
	ASYNC,
	INIT
};

enum class Action {
	SLEEP,
	PUT,
	GET,
	START,
	WAIT,
	SET_MBOX,   //Set receiver for mbox 
	UNSET_MBOX, //Un-set receiver for mbox
	DIE,
	END
};

const char* toString(const Action x) {

	switch(x) {
	case Action::END        : return "Success";
	case Action::SLEEP      : return "Sleep";
	case Action::PUT        : return "Put";
	case Action::GET        : return "Get";
	case Action::START      : return "Start";
	case Action::WAIT       : return "Wait";
	case Action::SET_MBOX   : return "Set Mbox";
	case Action::UNSET_MBOX : return "Unset Mbox";
	case Action::DIE        : return "Die!";
	};
	return "";
}


struct Step {
	double relTime; //Time relative to Scenario startTime
	enum {STATE,ACTION} type;
	enum {LNK,SND,RCV} entity;
	Action actionType;
	bool newState;
};


struct Scenario {
	CommType type;
	double startTime;
	double duration;
	Action sndExpected;
	Action rcvExpected;
	std::vector<Step> steps;
};

std::string toString(const Scenario& s) {

	std::stringstream ss;
	ss<<"["<<s.startTime<<"s,"<<s.startTime+s.duration<<"s[: (";
	switch(s.type) {
		case CommType::SYNC :ss<<"SYNC";break;
		case CommType::ASYNC:ss<<"ASYNC";break;
		case CommType::INIT :ss<<"INIT";break;
	}
	ss<<") Expected: S:"<<toString(s.sndExpected)<<" R:"<<toString(s.rcvExpected)<<" Steps: ";
	for(const Step& step :s.steps) {
		ss<<"+"<<step.relTime<<"s:";
		switch(step.entity) {
			case Step::LNK: ss<<"LNK";break;
			case Step::SND: ss<<"SND";break;
			case Step::RCV: ss<<"RCV";break;
		}

		if(step.type==Step::STATE) {
			ss<<"->";
			if(step.newState)
				ss<<"ON";
			else
				ss<<"OFF";	
		} else {
			ss<<"."<<toString(step.actionType);
		}
		ss<<" ";
	}

	return ss.str().c_str();
}

std::vector<Scenario> scenarios;

sg4::Mailbox* mbox = nullptr;

class SendAgent {

  static int run;
  static int scenario;
  
  int id;    

  void send_message(const Scenario& s) {
    double send_value;
    sg4::CommPtr comm=nullptr;
    CommType type=s.type;
    Action expected=s.sndExpected;
    double endTime=s.startTime+s.duration;
    double currRelTime=0;
    send_value=endTime;
    int stepIndex=0;
    sg4::this_actor::sleep_until(s.startTime);
    for(;stepIndex<s.steps.size();stepIndex++) {
        const Step& step=s.steps[stepIndex];
	if(step.entity!=Step::SND || step.type!=Step::ACTION)
		continue;

	try {
	  sg4::this_actor::sleep_until(s.startTime+step.relTime);
        } catch( std::exception& e) {
    	  XBT_DEBUG("During Sleep, failed to send message because of a %s exception (%s)", typeid(e).name(), e.what());
          break;
	}

	//Perform the action
	try {
	  switch(step.actionType) {
		case Action::PUT:
		switch(type){
			case CommType::SYNC:  mbox->put(&send_value, MsgSize);break;
			case CommType::ASYNC: comm=mbox->put_async(&send_value, MsgSize);break;
			case CommType::INIT:  comm=mbox->put_init(&send_value, MsgSize);break;
		};break;
		case Action::START:
			comm->start();break;
		case Action::WAIT:
			comm->wait();break;
		default: xbt_die("Not a valid action for SND");
	  }
	} catch( std::exception& e) {
    		XBT_DEBUG("During %s, failed to send message because of a %s exception (%s)", toString(step.actionType),typeid(e).name(), e.what());
        	break;
	}
    }

    try {
      sg4::this_actor::sleep_until(endTime);
    } catch( std::exception& e) {
      XBT_DEBUG("During Sleep, failed to send message because of a %s exception (%s)", typeid(e).name(), e.what());
    }
    
    Action outcome=Action::END;
    std::string scenarioString=toString(s);
    if(stepIndex<s.steps.size()) {
    	const Step& step=s.steps[stepIndex];
    	assert(step.entity==Step::SND && step.type==Step::ACTION);
	outcome=step.actionType;
    }

    if(outcome!=expected ) {
	XBT_ERROR("Expected %s but got %s in %s",toString(expected),toString(outcome),scenarioString.c_str());
    } else {
	XBT_DEBUG("OK: %s",scenarioString.c_str());
    }
    sg4::this_actor::sleep_until(endTime);
  }

public:

  explicit SendAgent(int id) : id(id) {}


  void operator()()  {
    run++;
    XBT_DEBUG("Host %i starts run %i and scenario %i.",id,run,scenario);
    while(scenario<scenarios.size()) {
	Scenario& s=scenarios[scenario];
	scenario++;
	send_message(s);
    }
  }

};

int SendAgent::run=0;
int SendAgent::scenario=0;


/*************************************************************************************************/

class ReceiveAgent {

  static int run;
  static int scenario;
  
  int id;    

  void receive_message(const Scenario& s) {
    sg4::CommPtr comm=nullptr;
    CommType type=s.type;
    Action expected=s.rcvExpected;
    double endTime=s.startTime+s.duration;
    double currRelTime=0;
    double *receive_ptr=nullptr;
    int stepIndex=0;
    sg4::this_actor::sleep_until(s.startTime);
    for(;stepIndex<s.steps.size();stepIndex++) {
        const Step& step=s.steps[stepIndex];
	if(step.entity!=Step::RCV || step.type!=Step::ACTION)
		continue;

	try {
	  sg4::this_actor::sleep_until(s.startTime+step.relTime);
        } catch( std::exception& e) {
    	  XBT_DEBUG("During Sleep, failed to receive message because of a %s exception (%s)", typeid(e).name(), e.what());
          break;
	}

	//Perform the action
	try {
	  switch(step.actionType) {
		case Action::GET:
		switch(type){
			case CommType::SYNC:  receive_ptr=mbox->get<double>();break;
			case CommType::ASYNC: comm=mbox->get_async(&receive_ptr);break;
			case CommType::INIT:  comm=mbox->get_init()->set_dst_data((void**)(&receive_ptr));break;
		};break;
		case Action::START:
			comm->start();break;
		case Action::WAIT:
			comm->wait();break;
		case Action::SET_MBOX:
			mbox->set_receiver(sg4::Actor::self());break;
		case Action::UNSET_MBOX:
			mbox->set_receiver(nullptr);break;
		default: xbt_die("Not a valid action for RCV");
	  }
	} catch( std::exception& e) {
    		XBT_DEBUG("During %s, failed to receive message because of a %s exception (%s)", toString(step.actionType),typeid(e).name(), e.what());
        	break;
	}
    }
    
    try {
      sg4::this_actor::sleep_until(endTime-.1);
    } catch( std::exception& e) {
      XBT_DEBUG("During Sleep, failed to send message because of a %s exception (%s)", typeid(e).name(), e.what());
    }
   
    Action outcome=Action::END;
    std::string scenarioString=toString(s);
    if(stepIndex<s.steps.size()) {
    	const Step& step=s.steps[stepIndex];
    	assert(step.entity==Step::RCV && step.type==Step::ACTION);
	outcome=step.actionType;
    } else if(receive_ptr==nullptr) {
	XBT_ERROR("Received address is NULL in %s",scenarioString.c_str());
    } else if(*receive_ptr!=endTime) {
	XBT_ERROR("Received value invalid: expected %f but got %f in %s",endTime, *receive_ptr,scenarioString.c_str());
    }

    if(outcome!=expected ) {
	XBT_ERROR("Expected %s but got %s in %s",toString(expected),toString(outcome),scenarioString.c_str());
    } else {
	XBT_DEBUG("OK: %s",scenarioString.c_str());
    }
    sg4::this_actor::sleep_until(endTime);
  }

public:

  explicit ReceiveAgent(int id) : id(id) {}


  void operator()()  {
    run++;
    XBT_DEBUG("Host %i starts run %i and scenario %i.",id,run,scenario);
    while(scenario<scenarios.size()) {
	Scenario& s=scenarios[scenario];
	scenario++;
	receive_message(s);
    }
  }

};

int ReceiveAgent::run=0;
int ReceiveAgent::scenario=0;

/*************************************************************************************************/

void on_host_state_change(sg4::Host const& host) {
  XBT_DEBUG("Host %s is now %s",host.get_cname(),host.is_on()?"ON ":"OFF"); 	
}

void on_link_state_change(sg4::Link const& link) {
  XBT_DEBUG("Link %s is now %s",link.get_cname(),link.is_on()?"ON ":"OFF"); 	
}

void addStateEvent(std::ostream& out, double date, bool isOn) {
  if(isOn)
	  out<<date<<" 1\n";
  else
	  out<<date<<" 0\n";
}

void prepareScenario(CommType type, double& startTime, double duration, std::ostream& sndP, std::ostream& rcvP, std::ostream& lnkP,  Action sndExpected, Action rcvExpected, std::vector<Step> steps) {

	//Update fault profiles
	for(Step& step : steps) {
		assert(step.relTime<duration);
		if(step.type!=Step::STATE)
			continue;
		int val=step.newState?1:0;
		switch(step.entity){
			case Step::SND:sndP<<startTime+step.relTime<<" "<<val<<std::endl;break;
			case Step::RCV:rcvP<<startTime+step.relTime<<" "<<val<<std::endl;break;
			case Step::LNK:lnkP<<startTime+step.relTime<<" "<<val<<std::endl;break;
		}
	}
	scenarios.push_back({type,startTime,duration,sndExpected,rcvExpected,steps});
	startTime+=duration;
}


/*************************************************************************************************/

//State profiles for the resources
std::string SndStateProfile; 
std::string RcvStateProfile; 
std::string LnkStateProfile; 


//A bunch of dirty macros to help readability (supposedly)
#define _MK(type,duration,sndExpected,rcvExpected,steps...) prepareScenario(CommType::type,startTime,duration,sndP,rcvP,lnkP,Action::sndExpected,Action::rcvExpected,{steps})
//Link
#define LOFF(relTime) {relTime,Step::STATE,Step::LNK,Action::END,false}
#define LON(relTime)  {relTime,Step::STATE,Step::LNK,Action::END,true}
//Sender
#define SOFF(relTime) {relTime,Step::STATE,Step::SND,Action::END,false}
#define SON(relTime)  {relTime,Step::STATE,Step::SND,Action::END,true}
#define SPUT(relTime) {relTime,Step::ACTION,Step::SND,Action::PUT,false}
#define SWAT(relTime) {relTime,Step::ACTION,Step::SND,Action::WAIT,false}
//Receiver
#define ROFF(relTime) {relTime,Step::STATE,Step::RCV,Action::END,false}
#define RON(relTime)  {relTime,Step::STATE,Step::RCV,Action::END,true}
#define RGET(relTime) {relTime,Step::ACTION,Step::RCV,Action::GET,false}
#define RWAT(relTime) {relTime,Step::ACTION,Step::RCV,Action::WAIT,false}

double buildScenarios() {

  //Build the set of simulation stages
  std::stringstream sndP;
  std::stringstream rcvP;
  std::stringstream lnkP;

  double startTime=0;

  //SYNC use cases
  /*
  //All good  
  _MK(SYNC,1,END,END,         SPUT(.2),         RGET(.4));
  _MK(SYNC,1,END,END,         RGET(.2),         SPUT(.4));
  //Receiver off
  _MK(SYNC,2,PUT,DIE,ROFF(.1),SPUT(.2),         RGET(.4),         RON(1));
  _MK(SYNC,2,PUT,DIE,         SPUT(.2),ROFF(.3),RGET(.4),         RON(1));
  _MK(SYNC,2,PUT,DIE,         SPUT(.2),         RGET(.4),ROFF(.5),RON(1));
  _MK(SYNC,2,PUT,DIE,         RGET(.2),         SPUT(.4),ROFF(.5),RON(1));
  //Sender off
  _MK(SYNC,2,DIE,GET,         SPUT(.2),SOFF(.3),RGET(.4),         SON(1));
  _MK(SYNC,2,DIE,GET,         SPUT(.2),         RGET(.4),SOFF(.5),SON(1));
  //Link off
  _MK(SYNC,2,PUT,GET,LOFF(.1),SPUT(.2),         RGET(.4),         LON(1));
  _MK(SYNC,2,PUT,GET,         SPUT(.2),LOFF(.3),RGET(.4),         LON(1));
  _MK(SYNC,2,PUT,GET,         SPUT(.2),         RGET(.4),LOFF(.5),LON(1));
  _MK(SYNC,2,PUT,GET,LOFF(.1),RGET(.2),         SPUT(.4),         LON(1));
  _MK(SYNC,2,PUT,GET,         RGET(.2),LOFF(.3),SPUT(.4),         LON(1));
  _MK(SYNC,2,PUT,GET,         RGET(.2),         SPUT(.4),LOFF(.5),LON(1));
  //ASYNC use cases
  //All good  
  _MK(ASYNC,2,END,END,         SPUT(.2),         SWAT(.4),         RGET(.6),         RWAT(.8));
  _MK(ASYNC,2,END,END,         SPUT(.2),         RGET(.4),         SWAT(.6),         RWAT(.8));
  _MK(ASYNC,2,END,END,         SPUT(.2),         RGET(.4),         RWAT(.6),         SWAT(.8));
  _MK(ASYNC,2,END,END,         RGET(.2),         SPUT(.4),         SWAT(.6),         RWAT(.8));
  _MK(ASYNC,2,END,END,         RGET(.2),         SPUT(.4),         RWAT(.6),         SWAT(.8));
  _MK(ASYNC,2,END,END,         RGET(.2),         RWAT(.4),         SPUT(.6),         SWAT(.8));
  //Receiver off
  _MK(ASYNC,2,WAIT,DIE,ROFF(.1),SPUT(.2),         SWAT(.4),                                    RON(1));
  _MK(ASYNC,2,WAIT,DIE,         SPUT(.2),ROFF(.3),SWAT(.4),                                    RON(1));
  _MK(ASYNC,2,WAIT,DIE,         RGET(.2),ROFF(.3),SPUT(.4),         SWAT(.6),                  RON(1));
 */
  _MK(ASYNC,2,WAIT,DIE,         SPUT(.2),         SWAT(.4),ROFF(.5),                           RON(1));
  _MK(ASYNC,2,WAIT,DIE,         SPUT(.2),         RGET(.4),ROFF(.5),SWAT(.6),                  RON(1));
  _MK(ASYNC,2,WAIT,DIE,         RGET(.2),         SPUT(.4),ROFF(.5),SWAT(.6),                  RON(1));
  _MK(ASYNC,2,WAIT,DIE,         RGET(.2),         RWAT(.4),ROFF(.5),SPUT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,DIE,         SPUT(.2),         SWAT(.4),         RGET(.6),ROFF(.7),         RON(1));
  _MK(ASYNC,2,WAIT,DIE,         SPUT(.2),         RGET(.4),         SWAT(.6),ROFF(.7),         RON(1));
  _MK(ASYNC,2,WAIT,DIE,         SPUT(.2),         RGET(.4),         RWAT(.6),ROFF(.7),SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,DIE,         RGET(.2),         SPUT(.4),         SWAT(.6),ROFF(.7),         RON(1));
  _MK(ASYNC,2,WAIT,DIE,         RGET(.2),         SPUT(.4),         RWAT(.6),ROFF(.7),SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,DIE,         RGET(.2),         RWAT(.4),         SPUT(.6),ROFF(.7),SWAT(.8),RON(1));
   //Sender off
  _MK(ASYNC,2,DIE,WAIT,SOFF(.1),RGET(.2),         RWAT(.4),                                    RON(1));
  _MK(ASYNC,2,DIE,WAIT,         RGET(.2),SOFF(.3),RWAT(.4),                                    RON(1));
  _MK(ASYNC,2,DIE,WAIT,         SPUT(.2),SOFF(.3),RGET(.4),         RWAT(.6),                  RON(1));
  _MK(ASYNC,2,DIE,WAIT,         RGET(.2),         RWAT(.4),SOFF(.5),                           RON(1));
  _MK(ASYNC,2,DIE,WAIT,         RGET(.2),         SPUT(.4),SOFF(.5),RWAT(.6),                  RON(1));
  _MK(ASYNC,2,DIE,WAIT,         SPUT(.2),         RGET(.4),SOFF(.5),RWAT(.6),                  RON(1));
  _MK(ASYNC,2,DIE,WAIT,         SPUT(.2),         SWAT(.4),SOFF(.5),RGET(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,DIE,WAIT,         RGET(.2),         RWAT(.4),         SPUT(.6),ROFF(.7),         RON(1));
  _MK(ASYNC,2,DIE,WAIT,         RGET(.2),         SPUT(.4),         RWAT(.6),ROFF(.7),         RON(1));
  _MK(ASYNC,2,DIE,WAIT,         RGET(.2),         SPUT(.4),         SWAT(.6),ROFF(.7),RWAT(.8),RON(1));
  _MK(ASYNC,2,DIE,WAIT,         SPUT(.2),         RGET(.4),         RWAT(.6),ROFF(.7),         RON(1));
  _MK(ASYNC,2,DIE,WAIT,         SPUT(.2),         RGET(.4),         SWAT(.6),ROFF(.7),RWAT(.8),RON(1));
  _MK(ASYNC,2,DIE,WAIT,         SPUT(.2),         SWAT(.4),         RGET(.6),ROFF(.7),RWAT(.8),RON(1));
  //Link off
  _MK(ASYNC,2,WAIT,WAIT,LOFF(.1),SPUT(.2),         SWAT(.4),         RGET(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,LOFF(.1),SPUT(.2),         RGET(.4),         SWAT(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,LOFF(.1),SPUT(.2),         RGET(.4),         RWAT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,LOFF(.1),RGET(.2),         SPUT(.4),         SWAT(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,LOFF(.1),RGET(.2),         SPUT(.4),         RWAT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,LOFF(.1),RGET(.2),         RWAT(.4),         SPUT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),LOFF(.3),SWAT(.4),         RGET(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),LOFF(.3),RGET(.4),         SWAT(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),LOFF(.3),RGET(.4),         RWAT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),LOFF(.3),SPUT(.4),         SWAT(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),LOFF(.3),SPUT(.4),         RWAT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),LOFF(.3),RWAT(.4),         SPUT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),         SWAT(.4),LOFF(.5),RGET(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),         RGET(.4),LOFF(.5),SWAT(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),         RGET(.4),LOFF(.5),RWAT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),         SPUT(.4),LOFF(.5),SWAT(.6),         RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),         SPUT(.4),LOFF(.5),RWAT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),         RWAT(.4),LOFF(.5),SPUT(.6),         SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),         SWAT(.4),         RGET(.6),LOFF(.7),RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),         RGET(.4),         SWAT(.6),LOFF(.7),RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         SPUT(.2),         RGET(.4),         RWAT(.6),LOFF(.7),SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),         SPUT(.4),         SWAT(.6),LOFF(.7),RWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),         SPUT(.4),         RWAT(.6),LOFF(.7),SWAT(.8),RON(1));
  _MK(ASYNC,2,WAIT,WAIT,         RGET(.2),         RWAT(.4),         SPUT(.6),LOFF(.7),SWAT(.8),RON(1));
  

  SndStateProfile=sndP.str();
  RcvStateProfile=rcvP.str();
  LnkStateProfile=lnkP.str();

  return startTime+1;
}

/*************************************************************************************************/


int main(int argc, char* argv[])
{

  double endTime=buildScenarios();	
	
  sg4::Engine e(&argc, argv);

  mbox = e.mailbox_by_name_or_create("laposte");

  sg4::NetZone *zone=sg4::create_full_zone("Top");

  pr::Profile* profileSenderHost=pr::ProfileBuilder::from_string("sender_profile",SndStateProfile,0);
  sg4::Host *senderHost=zone->create_host("senderHost",HostComputePower)->set_state_profile(profileSenderHost)->seal();
  sg4::ActorPtr sender=sg4::Actor::create("sender", senderHost,SendAgent(0));
  sender->set_auto_restart(true);

  pr::Profile* profileReceiverHost=pr::ProfileBuilder::from_string("receiver_profile",RcvStateProfile,0);
  sg4::Host *receiverHost=zone->create_host("receiverHost",HostComputePower)->set_state_profile(profileReceiverHost)->seal();
  sg4::ActorPtr receiver=sg4::Actor::create("receiver", receiverHost,ReceiveAgent(1));
  receiver->set_auto_restart(true);


  pr::Profile* profileLink=pr::ProfileBuilder::from_string("link_profile",LnkStateProfile,0);
  sg4::Link *link=zone->create_link("link",LinkBandwidth)->set_latency(LinkLatency)->set_state_profile(profileLink)->seal();

  zone->add_route(senderHost->get_netpoint(),receiverHost->get_netpoint(),nullptr,nullptr,{sg4::LinkInRoute{link}},false);
  zone->seal();
  
  sg4::Host::on_state_change.connect(on_host_state_change);
  sg4::Link::on_state_change_cb(on_link_state_change);
 
  //For now, just attach mbox to the receiver 
  mbox->set_receiver(receiver);

  e.run_until(endTime);

  return 0;
}
