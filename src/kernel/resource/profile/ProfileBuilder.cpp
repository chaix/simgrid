/* Copyright (c) 2004-2022. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "simgrid/kernel/ProfileBuilder.hpp"
#include "src/kernel/resource/profile/Profile.hpp"

namespace simgrid {
namespace kernel {
namespace profile {

Profile* ProfileBuilder::from_string(const std::string& name, const std::string& input, double periodicity)
{
  return Profile::from_string(name, input, periodicity);
}

Profile* ProfileBuilder::from_file(const std::string& path)
{
  return Profile::from_file(path);
}

Profile* ProfileBuilder::from_id(int id)
{
  return Profile::from_id(id);
}

void ProfileBuilder::on_profile_exhaustion_cb(const std::function<void(double,int,std::vector<DatedValue>&,GeneratorOutcome&)>& cb) 
{
  Profile::on_exhaustion.connect(cb); 
}

} // namespace profile
} // namespace kernel
} // namespace simgrid
