/**********************************************************************/
/* File generated by src/simix/simcalls.py from src/simix/simcalls.in */
/*                                                                    */
/*                    DO NOT EVER CHANGE THIS FILE                    */
/*                                                                    */
/* change simcalls specification in src/simix/simcalls.in             */
/* Copyright (c) 2014-2022. The SimGrid Team. All rights reserved.    */
/**********************************************************************/

/*
 * Note that the name comes from http://en.wikipedia.org/wiki/Popping
 * Indeed, the control flow is doing a strange dance in there.
 *
 * That's not about http://en.wikipedia.org/wiki/Poop, despite the odor :)
 */

#include <simgrid/config.h>
#include <simgrid/host.h>
#include <xbt/base.h>
#if SIMGRID_HAVE_MC
#include "src/mc/mc_forward.hpp"
#endif
#include "src/kernel/activity/ConditionVariableImpl.hpp"
#include "src/kernel/actor/SimcallObserver.hpp"
#include "src/kernel/context/Context.hpp"

XBT_LOG_EXTERNAL_DEFAULT_CATEGORY(simix);

using simgrid::simix::Simcall;
/** @brief Simcalls' names (generated from src/simix/simcalls.in) */
constexpr std::array<const char*, simgrid::simix::NUM_SIMCALLS> simcall_names{{
    "Simcall::NONE",
    "Simcall::RUN_KERNEL",
    "Simcall::RUN_BLOCKING",
}};

/** @private
 * @brief (in kernel mode) unpack the simcall and activate the handler
 *
 * This function is generated from src/simix/simcalls.in
 */
void simgrid::kernel::actor::ActorImpl::simcall_handle(int times_considered)
{
  XBT_DEBUG("Handling simcall %p: %s", &simcall_, SIMIX_simcall_name(simcall_));
  if (simcall_.observer_ != nullptr)
    simcall_.observer_->prepare(times_considered);
  if (context_->wannadie())
    return;
  switch (simcall_.call_) {
    case Simcall::RUN_KERNEL:
      SIMIX_run_kernel(simgrid::simix::unmarshal<std::function<void()> const*>(simcall_.args_[0]));
      simcall_answer();
      break;

    case Simcall::RUN_BLOCKING:
      SIMIX_run_blocking(simgrid::simix::unmarshal<std::function<void()> const*>(simcall_.args_[0]));
      break;

    case Simcall::NONE:
      throw std::invalid_argument(simgrid::xbt::string_printf("Asked to do the noop syscall on %s@%s",
                                                              get_cname(),
                                                              sg_host_get_name(get_host())));
    default:
      THROW_IMPOSSIBLE;
  }
}
